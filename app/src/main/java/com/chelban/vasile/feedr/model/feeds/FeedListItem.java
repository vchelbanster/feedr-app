package com.chelban.vasile.feedr.model.feeds;

import android.text.Html;

import com.chelban.vasile.feedr.model.earthquakes.Coordinate;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.chelban.vasile.feedr.ui.view.FieldViewItem;
import com.chelban.vasile.feedr.util.TextUtils;
import com.rometools.rome.feed.synd.SyndEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class FeedListItem implements DataListItem {
    private SyndEntry mSyndEntry;

    public FeedListItem() {
    }

    public FeedListItem(SyndEntry syndEntry) {
        this.mSyndEntry = syndEntry;
    }

    @Override
    public String getTitle() {
        return mSyndEntry != null ? mSyndEntry.getTitle() : null;
    }

    @Override
    public String getDetailedDescription() {
        return mSyndEntry != null ? (mSyndEntry.getDescription() != null ?
                Html.fromHtml(mSyndEntry.getDescription().getValue()).toString() : null) : null;
    }

    @Override
    public String getThumbnailUri() {
        return null;
    }

    @Override
    public long getDateTime() {
        return mSyndEntry != null ? (mSyndEntry.getPublishedDate() != null ?
                mSyndEntry.getPublishedDate().getTime() : System.currentTimeMillis()) : System.currentTimeMillis();
    }

    @Override
    public Character getHybridThumbChar() {
        return mSyndEntry.getTitle().charAt(0);
    }

    @Override
    public Coordinate getLocation() {
        return null;
    }

    @Override
    public List<FieldViewItem> getItemFields() {
        final List<FieldViewItem> fieldViewItems = new ArrayList<FieldViewItem>();
        fieldViewItems.add(new FieldViewItem("Title", mSyndEntry.getTitle(), FieldViewItem.FieldType.MULTI_LINE, 2));
        if (!android.text.TextUtils.isEmpty(mSyndEntry.getAuthor())) {
            fieldViewItems.add(new FieldViewItem("Author", mSyndEntry.getAuthor(), FieldViewItem.FieldType.SINGLE_LINE));
        }
        TextUtils textUtils = new TextUtils();
        fieldViewItems.add(new FieldViewItem("Published on", textUtils.formatLongDateTime(mSyndEntry.getPublishedDate().getTime()),
                FieldViewItem.FieldType.SINGLE_LINE));
        if (mSyndEntry.getUpdatedDate() != null) {
            fieldViewItems.add(new FieldViewItem("Updated on", textUtils.formatLongDateTime(mSyndEntry.getUpdatedDate().getTime()),
                    FieldViewItem.FieldType.SINGLE_LINE));
        }
        final String description = mSyndEntry.getDescription().getType() == null ?
                mSyndEntry.getDescription().getValue() :
                Html.fromHtml(mSyndEntry.getDescription().getValue()).toString();
        fieldViewItems.add(new FieldViewItem(null, description,
                FieldViewItem.FieldType.MULTI_LINE, 10));

        if (android.text.TextUtils.isEmpty(mSyndEntry.getLink())) {
            fieldViewItems.add(new FieldViewItem("More details", mSyndEntry.getLink(),
                    FieldViewItem.FieldType.SINGLE_LINE));
        }

        return fieldViewItems;
    }

    public SyndEntry getSyndEntry() {
        return mSyndEntry;
    }

    public void setSyndEntry(SyndEntry entry) {
        this.mSyndEntry = entry;
    }


}
