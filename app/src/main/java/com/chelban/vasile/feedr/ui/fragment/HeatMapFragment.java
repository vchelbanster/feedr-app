package com.chelban.vasile.feedr.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.ui.activity.RoboActionBarActivity;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.chelban.vasile.feedr.util.FragmentUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class HeatMapFragment extends BaseMapFragment {
    private List<LatLng> heatMapList;
    private List<DataListItem> heatMapListInput;
    private GoogleMap mGoogleMap;
    private HeatmapTileProvider heatmapTileProvider;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mGoogleMap = getMap();
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.setMyLocationEnabled(false);
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (heatmapTileProvider == null) {
                    HeatMapProcessor task = new HeatMapProcessor();
                    task.execute();
                } else {
                    mGoogleMap.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
                }
            }
        });
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final ActionBar actionBar = ((RoboActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.heat_map_title);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (heatmapTileProvider != null) {
            mGoogleMap.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
        }
    }

    public List<LatLng> getHeatMapList() {
        return heatMapList;
    }

    public void setHeatMapList(List<DataListItem> heatMapList) {
        this.heatMapListInput = heatMapList;

    }

    private class HeatMapProcessor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (heatMapListInput != null) {
                heatMapList = new ArrayList<LatLng>();
                for (final DataListItem item : heatMapListInput) {
                    if (item.getLocation() != null) {
                        heatMapList.add(new LatLng(item.getLocation().getLatitude(), item.getLocation().getLongitude()));
                    }
                }
                heatmapTileProvider = new HeatmapTileProvider.Builder()
                        .data(heatMapList)
                        .build();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (heatMapList != null) {
                mGoogleMap.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(heatMapList.get(0)));
            }
        }
    }
}
