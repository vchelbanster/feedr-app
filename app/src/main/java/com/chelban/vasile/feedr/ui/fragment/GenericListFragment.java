package com.chelban.vasile.feedr.ui.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;
import com.chelban.vasile.feedr.network.EarthquakeFeedType;
import com.chelban.vasile.feedr.network.FeedID;
import com.chelban.vasile.feedr.network.NetworkFacade;
import com.chelban.vasile.feedr.network.NetworkResponseListener;
import com.chelban.vasile.feedr.network.adapter.EarthquakesResponseAdapter;
import com.chelban.vasile.feedr.network.adapter.FeedResponseAdapter;
import com.chelban.vasile.feedr.ui.activity.RoboActionBarActivity;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.chelban.vasile.feedr.ui.adapter.GenericListAdapter;
import com.chelban.vasile.feedr.util.FragmentUtils;
import com.google.inject.Inject;
import com.rometools.rome.feed.synd.SyndFeed;

import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import roboguice.RoboGuice;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class GenericListFragment extends BaseDataFragment implements SwipeRefreshLayout.OnRefreshListener,
        ActionBar.OnNavigationListener {
    private static final int EARTHQUAKES_SIGNIFICANT_MONTH = 0;
    private static final int EARTHQUAKES_HIGH_WEEK = 1;
    private static final int EARTHQUAKES_ALL_WEEK = 2;
    @Inject
    private GenericListAdapter mListAdapter;
    private DataSourceType mSourceType;
    @Inject
    private NetworkFacade mNetworkFacade;
    private ListView mListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FragmentUtils mFragmentUtils = new FragmentUtils();
    private String[] mDataOptions;
    private int mSelectedDataSource = EARTHQUAKES_SIGNIFICANT_MONTH;
    private MenuItem heatMapMenuItem;

    public static GenericListFragment newInstance(DataSourceType sourceType) {
        GenericListFragment fragment = new GenericListFragment();
        fragment.setSourceType(sourceType);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (navigationDrawerFragment.isDrawerOpen()) {
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        } else {
            setupDataSourceOptions();
            inflater.inflate(R.menu.item_list_menu, menu);
            heatMapMenuItem = menu.findItem(R.id.heat_map);
            if (heatMapMenuItem != null) {
                heatMapMenuItem.setVisible(mListAdapter.hasAnyLocation());
                heatMapMenuItem.setEnabled(mListAdapter.hasAnyLocation());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.heat_map) {
            final HeatMapFragment heatMapFragment = RoboGuice.getInjector(getActivity()).getInstance(HeatMapFragment.class);
            heatMapFragment.setHeatMapList(mListAdapter.getListItems());
            mFragmentUtils.attachFragment(getFragmentManager(), R.id.main_container, heatMapFragment, true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean isChildScreen() {
        return false;
    }

    private void loadData() {
        switch (mSourceType) {
            case EARTHQUAKES: {
                mSwipeRefreshLayout.setRefreshing(true);
                final EarthquakeFeedType feedType;
                switch (mSelectedDataSource) {
                    case EARTHQUAKES_SIGNIFICANT_MONTH: {
                        feedType = EarthquakeFeedType.SIGNIFICANT_MONTH;
                        break;
                    }
                    case EARTHQUAKES_HIGH_WEEK: {
                        feedType = EarthquakeFeedType.HIGH_MAGNITUDE_WEEK;
                        break;
                    }
                    case EARTHQUAKES_ALL_WEEK: {
                        feedType = EarthquakeFeedType.ALL_WEEK;
                        break;
                    }
                    default: {
                        feedType = EarthquakeFeedType.SIGNIFICANT_MONTH;
                    }
                }
                mNetworkFacade.getEarthquakesByType(feedType, new NetworkResponseListener<CollectionContainer>() {
                    @Override
                    public void onSuccess(CollectionContainer response) {
                        final EarthquakesResponseAdapter responseAdapter = new EarthquakesResponseAdapter(response.getFeatures());
                        updateDataSet(responseAdapter.getProcessedResponse());
                        if (heatMapMenuItem != null) {
                            heatMapMenuItem.setVisible(mListAdapter.hasAnyLocation());
                            heatMapMenuItem.setEnabled(mListAdapter.hasAnyLocation());
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(String errMsg) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        errMsg = errMsg != null ? errMsg : getString(R.string.exception_generic_msg);
                        Crouton.makeText(getActivity(), errMsg, Style.ALERT).show();
                    }
                });
                break;
            }
            case MUBALOO_FEED: {
                mSwipeRefreshLayout.setRefreshing(true);
                mNetworkFacade.getFeed(FeedID.MUBALOO, new NetworkResponseListener<SyndFeed>() {
                    @Override
                    public void onSuccess(SyndFeed response) {
                        FeedResponseAdapter responseAdapter = new FeedResponseAdapter(response.getEntries());
                        updateDataSet(responseAdapter.getProcessedResponse());
                        if (heatMapMenuItem != null) {
                            heatMapMenuItem.setVisible(mListAdapter.hasAnyLocation());
                            heatMapMenuItem.setEnabled(mListAdapter.hasAnyLocation());
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(String errorMsg) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        errorMsg = errorMsg != null ? errorMsg : getString(R.string.exception_generic_msg);
                        Crouton.makeText(getActivity(), errorMsg, Style.ALERT).show();
                    }
                });
                break;
            }
        }
    }

    private void updateDataSet(final List<DataListItem> listItems) {
        if (mListAdapter == null) {
            mListAdapter = RoboGuice.getInjector(getActivity()).getInstance(GenericListAdapter.class);
            mListView.setAdapter(mListAdapter);
        }
        mListAdapter.setListItems(listItems);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        mListView = (ListView) fragmentView.findViewById(R.id.list_view);
        mListView.setFastScrollEnabled(true);
        mSwipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipe_container);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SimpleItemDetailsFragment fragment = RoboGuice.getInjector(getActivity()).getInstance(SimpleItemDetailsFragment.class);
                final Bundle args = new Bundle();
                args.putSerializable(SimpleItemDetailsFragment.ITEM_DETAILS_KEY, mListAdapter.getItem(position));
                fragment.setArguments(args);
                mFragmentUtils.attachFragment(getFragmentManager(), R.id.main_container, fragment, true);
            }
        });
        return fragmentView;
    }

    @Override
    public boolean onNavigationItemSelected(int position, long itemId) {
        if (mSelectedDataSource != position) {
            mSelectedDataSource = position;
            loadData();
            return true;
        } else {
            return false;
        }
    }

    private void setupDataSourceOptions() {
        final ActionBar actionBar = getSupportActionBar();
        if (mSourceType == DataSourceType.EARTHQUAKES) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            actionBar.setDisplayShowTitleEnabled(false);
            if (mDataOptions == null) {
                mDataOptions = getResources().getStringArray(R.array.earthquakes_data_options);
            }
            final SpinnerAdapter spinnerAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, mDataOptions);

            actionBar.setListNavigationCallbacks(spinnerAdapter, this);
            actionBar.setSelectedNavigationItem(mSelectedDataSource);

        } else {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (mListAdapter == null || mListAdapter.getCount() == 0) {
            loadData();
        }
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright,
                R.color.holo_green_light,
                R.color.holo_orange_light,
                R.color.holo_red_light);
        if (mListAdapter != null) {
            mListView.setAdapter(mListAdapter);
            if (heatMapMenuItem != null) {
                heatMapMenuItem.setEnabled(mListAdapter.hasAnyLocation());
                heatMapMenuItem.setVisible(mListAdapter.hasAnyLocation());
            }
        }
        setHasOptionsMenu(true);
    }

    public DataSourceType getSourceType() {
        return mSourceType;
    }

    public void setSourceType(DataSourceType sourceType) {
        this.mSourceType = sourceType;
    }


    @Override
    public void onRefresh() {
        loadData();
    }


    private ActionBar getSupportActionBar() {
        return ((RoboActionBarActivity) getActivity()).getSupportActionBar();
    }

}
