package com.chelban.vasile.feedr.ui.view;

import java.io.Serializable;

/**
 * Created by Vasile Chelban on 7/22/2014.
 */
public class FieldViewItem implements Serializable {
    private int itemId = -1;
    private String title;
    private String value;
    private int maxLines;
    private FieldType fieldType;

    public FieldViewItem(int itemId, String title, String value, FieldType fieldType, int maxLines) {
        this.itemId = itemId;
        this.title = title;
        this.value = value;
        this.fieldType = fieldType;
        this.maxLines = maxLines;
    }

    public FieldViewItem(String title, String value, FieldType fieldType, int maxLines) {
        this(-1, title, value, fieldType, maxLines);
    }

    public FieldViewItem(String title, String value, FieldType fieldType) {
        this(-1, title, value, fieldType, 1);
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getMaxLines() {
        return maxLines;
    }

    public void setMaxLines(int maxLines) {
        this.maxLines = maxLines;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }


    public enum FieldType {
        SINGLE_LINE,
        MULTI_LINE
    }
}
