package com.chelban.vasile.feedr.util;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by Vasile Chelban on 7/20/2014.
 */
public final class ImageLoadingUtil {

    public static void initImageLoader(final Context appContext) {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();
        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(appContext)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
    }

    public void displayImage(final String imageUrl, final ImageViewAware imageViewAware, ImageLoadingListener imageLoadingListener) {
        if (imageViewAware != null) {
            try {
                ImageLoader.getInstance().displayImage(imageUrl, imageViewAware, imageLoadingListener);
            } catch (IllegalStateException ignored) {
            } catch (IllegalArgumentException ignored) {
            }
        }
    }

}
