package com.chelban.vasile.feedr.network;


import com.chelban.vasile.feedr.exception.GenericException;
import com.chelban.vasile.feedr.exception.NetworkException;
import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Vasile Chelban on 7/18/2014.
 */
public interface RestfulClient {

    @GET(NetworkConsts.EARTHQUAKE_GATEWAY_PREFIX)
    CollectionContainer getEarthquakes(@Path(NetworkConsts.EARTHQUAKE_FEED_TYPE) String feedSuffix)
            throws GenericException, NetworkException;


}
