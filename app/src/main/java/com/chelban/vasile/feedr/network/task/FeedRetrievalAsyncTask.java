package com.chelban.vasile.feedr.network.task;


import android.os.AsyncTask;

import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;
import com.chelban.vasile.feedr.network.EarthquakeFeedType;
import com.chelban.vasile.feedr.network.FeedID;
import com.chelban.vasile.feedr.network.NetworkConsts;
import com.chelban.vasile.feedr.network.NetworkResponseListener;
import com.google.inject.Inject;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import roboguice.util.Ln;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class FeedRetrievalAsyncTask extends AsyncTask<Void, Void, SyndFeed> {
    private NetworkResponseListener<SyndFeed> responseListener;
    private FeedID feedID;
    private String errMsg;
    @Inject
    private SyndFeedInput feedInput;

    @Override
    protected SyndFeed doInBackground(Void... params) {
        final String feedUrl;
        switch (feedID) {
            case MUBALOO: {
                feedUrl = NetworkConsts.MUBALOO_SERVER_URL;
                break;
            }
            default: {
                feedUrl = NetworkConsts.MUBALOO_SERVER_URL;
                break;
            }
        }
        SyndFeed response = null;
        try {
            final XmlReader xmlReader = new XmlReader(new URL(feedUrl));
            response = feedInput.build(xmlReader);
        } catch (MalformedURLException e) {
            Ln.w(e.getMessage(), e);
            errMsg = e.getMessage();
        } catch (FeedException e) {
            Ln.w(e.getMessage(), e);
            errMsg = e.getMessage();
        } catch (IOException e) {
            Ln.w(e.getMessage(), e);
            errMsg = e.getMessage();
        }

        return response;
    }

    @Override
    protected void onPostExecute(SyndFeed syndFeed) {
        if (syndFeed != null) {
            responseListener.onSuccess(syndFeed);
        } else {
            responseListener.onFailure(errMsg);
        }
    }

    public void setResponseListener(NetworkResponseListener<SyndFeed> responseListener) {
        this.responseListener = responseListener;
    }

    public FeedID getFeedID() {
        return feedID;
    }

    public void setFeedID(FeedID feedID) {
        this.feedID = feedID;
    }
}
