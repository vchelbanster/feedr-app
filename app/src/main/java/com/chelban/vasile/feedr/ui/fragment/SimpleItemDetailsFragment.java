package com.chelban.vasile.feedr.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.ui.activity.RoboActionBarActivity;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.chelban.vasile.feedr.ui.view.FieldListView;
import com.chelban.vasile.feedr.util.FragmentUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import roboguice.RoboGuice;

/**
 * Created by Vasile Chelban on 7/22/2014.
 */
public class SimpleItemDetailsFragment extends BaseDataFragment {
    public static final String ITEM_DETAILS_KEY = "item_details";
    private FieldListView mFieldListView;
    private DataListItem mDataItem;
    private MapView mapView;
    private GoogleMap googleMap;
    private LatLng itemLocation;
    private FragmentUtils fragmentUtils = new FragmentUtils();
    private MarkerOptions markerOption;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null && args.size() > 0) {
            mDataItem = (DataListItem) args.getSerializable(ITEM_DETAILS_KEY);
        }
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.simple_item_details_layout, container, false);
        mFieldListView = (FieldListView) view.findViewById(R.id.field_list);
        if (mDataItem != null) {
            mFieldListView.initFieldViewItems(mDataItem.getItemFields());
            if (mDataItem.getLocation() != null) {
                itemLocation = new LatLng(mDataItem.getLocation().getLatitude(), mDataItem.getLocation().getLongitude());
            }
            final ActionBar actionBar = ((RoboActionBarActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setTitle(mDataItem.getTitle());
            }
        }

        return view;
    }

    private GoogleMap.OnMapLoadedCallback mapLoadedCallback = new GoogleMap.OnMapLoadedCallback() {
        @Override
        public void onMapLoaded() {
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(itemLocation));
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.item_map_view);
        if (itemLocation != null) {
            mapView.setVisibility(View.VISIBLE);
            mapView.onCreate(savedInstanceState);
            googleMap = mapView.getMap();
            MapsInitializer.initialize(getActivity());
            if (googleMap != null) {
                googleMap.setOnMapLoadedCallback(mapLoadedCallback);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        final FullViewMapFragment mapFragment = RoboGuice.getInjector(getActivity()).getInstance(FullViewMapFragment.class);
                        mapFragment.setLocation(itemLocation);
                        fragmentUtils.attachFragment(getFragmentManager(), R.id.main_container, mapFragment, true);
                    }
                });
                if (markerOption == null) {
                    markerOption = new MarkerOptions()
                            .draggable(false)
                            .position(itemLocation);
                }
                googleMap.addMarker(markerOption);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (itemLocation != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (itemLocation != null) {
            mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (itemLocation != null) {
            mapView.onLowMemory();
        }
    }
}
