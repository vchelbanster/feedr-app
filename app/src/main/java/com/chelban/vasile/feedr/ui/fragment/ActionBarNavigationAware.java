package com.chelban.vasile.feedr.ui.fragment;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public interface ActionBarNavigationAware {
    boolean isChildScreen();
}
