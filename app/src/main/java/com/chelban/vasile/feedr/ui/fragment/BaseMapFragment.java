package com.chelban.vasile.feedr.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.util.FragmentUtils;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by Vasile Chelban on 7/24/2014.
 */
public class BaseMapFragment extends  SupportMapFragment implements ActionBarNavigationAware {
    private FragmentUtils fragmentUtils = new FragmentUtils();
    private NavigationDrawerFragment navigationDrawerFragment;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navigationDrawerFragment =
                (NavigationDrawerFragment) fragmentUtils.getCurrentFragment(getFragmentManager(), R.id.navigation_drawer);
        if (navigationDrawerFragment != null) {
            navigationDrawerFragment.setDrawerIndicatorEnabled(!isChildScreen());
            navigationDrawerFragment.lockDrawer();
        }
    }

    @Override
    public void onStop() {
        if (navigationDrawerFragment != null) {
            navigationDrawerFragment.unlockDrawer();
        }
        super.onStop();
    }

    @Override
    public boolean isChildScreen() {
        return true;
    }


}
