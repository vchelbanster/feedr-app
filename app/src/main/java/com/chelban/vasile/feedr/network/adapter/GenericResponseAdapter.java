package com.chelban.vasile.feedr.network.adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public abstract class GenericResponseAdapter<INPUT, OUTPUT> {
    protected List<INPUT> mInputList;
    protected List<OUTPUT> mOutputList;

    public GenericResponseAdapter() {
        mInputList = new ArrayList<INPUT>();
    }

    public void setInputList(final List<INPUT> inputList) {
        mInputList.clear();
        mInputList.addAll(inputList);
    }

    public GenericResponseAdapter(final List<INPUT> inputList) {
        mInputList = new ArrayList<INPUT>();
        if (inputList != null) {
            mInputList.addAll(inputList);
        }
        mOutputList = new ArrayList<OUTPUT>();
    }


    public abstract List<OUTPUT> getProcessedResponse();

}
