package com.chelban.vasile.feedr.ui.adapter;

import com.chelban.vasile.feedr.model.earthquakes.Coordinate;
import com.chelban.vasile.feedr.ui.view.FieldViewItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public interface DataListItem extends Serializable{

    String getTitle();

    String getDetailedDescription();

    String getThumbnailUri();

    long getDateTime();

    Character getHybridThumbChar();

    Coordinate getLocation();

    List<FieldViewItem> getItemFields();

}
