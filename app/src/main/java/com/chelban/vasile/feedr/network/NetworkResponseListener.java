package com.chelban.vasile.feedr.network;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public interface NetworkResponseListener<T> {

    void onSuccess(T response);

    void onFailure(final String errorMsg);
}
