package com.chelban.vasile.feedr.model.earthquakes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class FeatureGeometry implements Serializable {
    private String type;
    @SerializedName("coordinates")
    private List<Double> sourceCoords;
    private Coordinate featureCoordinates;

    public FeatureGeometry() {
    }

    public Coordinate getFeatureCoordinates() {
        if (featureCoordinates == null && sourceCoords != null && sourceCoords.size() == 3) {
            featureCoordinates = new Coordinate(sourceCoords.get(0), sourceCoords.get(1), sourceCoords.get(2));
        }
        return featureCoordinates;
    }

    public void setFeatureCoordinates(Coordinate featureCoordinates) {
        this.featureCoordinates = featureCoordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
