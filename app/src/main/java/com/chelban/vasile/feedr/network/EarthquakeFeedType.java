package com.chelban.vasile.feedr.network;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public enum EarthquakeFeedType {
    SIGNIFICANT_MONTH,
    HIGH_MAGNITUDE_WEEK,
    ALL_WEEK
}
