package com.chelban.vasile.feedr.network;

import android.content.Context;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;
import com.chelban.vasile.feedr.network.task.EarthquakeRetrievalAsyncTask;
import com.chelban.vasile.feedr.network.task.FeedRetrievalAsyncTask;
import com.chelban.vasile.feedr.util.NetworkUtils;
import com.google.inject.Inject;
import com.rometools.rome.feed.synd.SyndFeed;

import roboguice.RoboGuice;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class NetworkFacadeImpl implements NetworkFacade {
    @Inject
    private Context mContext;

    @Override
    public void getEarthquakesByType(EarthquakeFeedType feedType, final NetworkResponseListener<CollectionContainer> responseListener) {
        if (responseListener != null) {
            if (checkNetworkAndDispatchError() && feedType != null) {
                EarthquakeRetrievalAsyncTask asyncTask = RoboGuice.getInjector(mContext).getInstance(EarthquakeRetrievalAsyncTask.class);
                asyncTask.setFeedType(feedType);
                asyncTask.setResponseListener(responseListener);
                asyncTask.execute();
            } else {
                responseListener.onFailure(mContext.getString(R.string.exception_no_network_msg));
            }
        }
    }

    @Override
    public void getFeed(FeedID feedID, NetworkResponseListener<SyndFeed> responseListener) {
        if (responseListener != null) {
            if (checkNetworkAndDispatchError() && feedID != null) {
                FeedRetrievalAsyncTask asyncTask = RoboGuice.getInjector(mContext).getInstance(FeedRetrievalAsyncTask.class);
                asyncTask.setFeedID(feedID);
                asyncTask.setResponseListener(responseListener);
                asyncTask.execute();
            } else {
                responseListener.onFailure(mContext.getString(R.string.exception_no_network_msg));
            }
        }
    }

    private boolean checkNetworkAndDispatchError() {
        return NetworkUtils.isNetworkConnectionAvailable(mContext);
    }
}
