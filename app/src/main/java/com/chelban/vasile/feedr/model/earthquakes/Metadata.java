package com.chelban.vasile.feedr.model.earthquakes;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class Metadata {
    private long generated;
    private String url;
    private String title;
    private int status;
    private String api;
    private int count;

    public Metadata() {
    }

    public Metadata(int generated, String url, String title, int status, String api, int count) {
        this.generated = generated;
        this.url = url;
        this.title = title;
        this.status = status;
        this.api = api;
        this.count = count;
    }

    public long getGenerated() {
        return generated;
    }

    public void setGenerated(long generated) {
        this.generated = generated;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
