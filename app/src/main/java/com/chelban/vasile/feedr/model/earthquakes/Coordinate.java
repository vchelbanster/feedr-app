package com.chelban.vasile.feedr.model.earthquakes;

import java.io.Serializable;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class Coordinate implements Serializable {
    private double longitude;
    private double latitude;
    private double depth;

    public Coordinate() {
    }

    public Coordinate(double longitude, double latitude, double depth) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.depth = depth;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }


}
