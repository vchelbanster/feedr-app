package com.chelban.vasile.feedr.di;


import android.content.Context;

import com.chelban.vasile.feedr.exception.GenericException;
import com.chelban.vasile.feedr.exception.NetworkException;
import com.chelban.vasile.feedr.network.NetworkConsts;
import com.chelban.vasile.feedr.network.RestfulClient;
import com.google.inject.Inject;
import com.google.inject.Provider;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import roboguice.RoboGuice;
import roboguice.util.Ln;

/**
 * A simple provider implementation for {@link RestfulClient}.
 * <p/>
 * Created by Vasile Chelban on 7/19/2014.
 */
public class RestfulClientProvider implements Provider<RestfulClient> {
    @Inject
    private Context mContext;

    @Override
    public RestfulClient get() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                //TODO switch logging off in release mode
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(NetworkConsts.EARTHQUAKES_SERVER_BASE_URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade requestFacade) {
                        requestFacade.addHeader("Connection", "close");
                        requestFacade.addHeader("Accept-Charset", "utf-8");
                    }
                })
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        Ln.e(cause.getCause());
                        if (cause.isNetworkError()) {
                            return RoboGuice.getInjector(mContext).getInstance(NetworkException.class);
                        } else {
                            return RoboGuice.getInjector(mContext).getInstance(GenericException.class);
                        }
                    }
                })
                .build();

        return restAdapter.create(RestfulClient.class);
    }
}
