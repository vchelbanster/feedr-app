package com.chelban.vasile.feedr.util;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.chelban.vasile.feedr.R;
import com.google.inject.Inject;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public final class FragmentUtils {
    @Inject
    private FragmentManager mFragmentManager;

    /**
     * Adds fragment to the activity layout or replaces an existing one (if specified) using the specified fragment id param.
     *
     * @param fragmentManager the actual {@link android.support.v4.app.FragmentManager} that will manage {@link android.support.v4.app.FragmentTransaction}s.
     * @param fragmentId      resource ID of the fragment container
     * @param fragment        fragment to be attached to activity.
     * @param addToBackStack  if <code>false</code> - it will replace any existing fragment on the stack (only the last one); <br />
     *                        <code>true</code> - adds the fragment to existing stack.
     */
    public void attachFragment(final FragmentManager fragmentManager, final int fragmentId, Fragment fragment, boolean addToBackStack) {
        final Fragment currentFragment = getCurrentFragment(fragmentManager, fragmentId);
        if (currentFragment == fragment) {
//            just in case the fragment to be attached it was attached before using this FragmentManager
            return;
        }

        if (currentFragment == null || (currentFragment.isAdded())) {
            FragmentTransaction ft = fragmentManager.beginTransaction();

            ft.setCustomAnimations(0, 0);

            ft.replace(fragmentId, fragment);
            if (addToBackStack) {
                ft.addToBackStack(null);
            }
            ft.commit();
        }
    }

    public void attachFragment(final Fragment fragment, final boolean addToBackStack) {
        attachFragment(mFragmentManager, R.id.main_container, fragment, addToBackStack);
    }

    public Fragment getCurrentFragment(final FragmentManager fragmentManager, final int fragmentHolderId) {
        return fragmentManager.findFragmentById(fragmentHolderId);
    }


    public Fragment getCurrentFragment() {
        return getCurrentFragment(mFragmentManager, R.id.main_container);
    }

}
