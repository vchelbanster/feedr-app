package com.chelban.vasile.feedr.network.task;

import android.os.AsyncTask;

import com.chelban.vasile.feedr.exception.GenericException;
import com.chelban.vasile.feedr.exception.NetworkException;
import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;
import com.chelban.vasile.feedr.network.EarthquakeFeedType;
import com.chelban.vasile.feedr.network.NetworkConsts;
import com.chelban.vasile.feedr.network.NetworkResponseListener;
import com.chelban.vasile.feedr.network.RestfulClient;
import com.google.inject.Inject;

import roboguice.util.Ln;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class EarthquakeRetrievalAsyncTask extends AsyncTask<Void, Void, CollectionContainer> {
    private NetworkResponseListener<CollectionContainer> responseListener;
    private EarthquakeFeedType feedType;
    private String errMsg;
    @Inject
    private RestfulClient mNetworkClient;

    @Override
    protected CollectionContainer doInBackground(Void... params) {
        final String feedSuffix;
        switch (feedType) {
            case ALL_WEEK: {
                feedSuffix = NetworkConsts.EARTHQUAKE_ALL_WEEK_URL;
                break;
            }
            case HIGH_MAGNITUDE_WEEK: {
                feedSuffix = NetworkConsts.EARTHQUAKE_HIGH_WEEK_URL;
                break;
            }
            case SIGNIFICANT_MONTH: {
                feedSuffix = NetworkConsts.EARTHQUAKE_SIGNIFICANT_MONTH_URL;
                break;
            }
            default: {
                feedSuffix = NetworkConsts.EARTHQUAKE_SIGNIFICANT_MONTH_URL;
                break;
            }
        }
        CollectionContainer response = null;
        try {
            response = mNetworkClient.getEarthquakes(feedSuffix);
        } catch (NetworkException e) {
            Ln.e(e.getMessage());
            errMsg = e.getMessage();
        } catch (GenericException e) {
            Ln.e(e.getMessage());
            errMsg = e.getMessage();
        }

        return response;
    }

    @Override
    protected void onPostExecute(CollectionContainer collectionContainer) {
        if (collectionContainer != null) {
            responseListener.onSuccess(collectionContainer);
        } else {
            responseListener.onFailure(errMsg);
        }
    }

    public void setResponseListener(NetworkResponseListener<CollectionContainer> responseListener) {
        this.responseListener = responseListener;
    }

    public void setFeedType(EarthquakeFeedType feedType) {
        this.feedType = feedType;
    }
}
