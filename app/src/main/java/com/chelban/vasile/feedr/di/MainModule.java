package com.chelban.vasile.feedr.di;

import com.chelban.vasile.feedr.network.NetworkFacade;
import com.chelban.vasile.feedr.network.NetworkFacadeImpl;
import com.chelban.vasile.feedr.network.RestfulClient;
import com.google.inject.AbstractModule;

/**
 * Created by Vasile Chelban on 7/18/2014.
 */
public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RestfulClient.class).toProvider(RestfulClientProvider.class);
        bind(NetworkFacade.class).to(NetworkFacadeImpl.class);
    }
}
