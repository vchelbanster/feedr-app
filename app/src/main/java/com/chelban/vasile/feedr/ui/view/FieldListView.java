package com.chelban.vasile.feedr.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chelban.vasile.feedr.R;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


/**
 * Created by Vasile Chelban on 7/22/2014.
 */
public class FieldListView extends LinearLayout {
    private List<FieldViewItem> mFieldViewItems;
    private boolean isInitialized;
    private LayoutInflater mLayoutInflater;

    public FieldListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInitialized) {
            initFormView();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public FieldListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInitialized) {
            initFormView();
        }
    }

    private void initFormView() {
        mFieldViewItems = new ArrayList<FieldViewItem>();
        setOrientation(VERTICAL);
        isInitialized = true;
        mLayoutInflater = LayoutInflater.from(getContext());
    }

    private LayoutParams separatorLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);

    public final void initFieldViewItems(final List<FieldViewItem> FieldViewItems) {
        mFieldViewItems.clear();
        mFieldViewItems.addAll(FieldViewItems);
        if (getChildCount() > 0) {
            removeAllViews();
        }
        final LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;

        ListIterator<FieldViewItem> iterator = mFieldViewItems.listIterator();
        while (iterator.hasNext()) {
            final FieldViewItem fieldViewItem = iterator.next();
            final LinearLayout itemLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.row_field_list, this, false);
            itemLayout.setId(fieldViewItem.getItemId());
            final TextView label = (TextView) itemLayout.findViewById(R.id.item_title);
            final TextView itemValue = (TextView) itemLayout.findViewById(R.id.item_value);
            if (TextUtils.isEmpty(fieldViewItem.getTitle())) {
                label.setVisibility(View.GONE);
                final LayoutParams fullFieldParams = new LayoutParams(params.width, params.height);
                fullFieldParams.gravity = params.gravity;
                itemValue.setGravity(Gravity.LEFT);
                itemValue.setLayoutParams(fullFieldParams);
            } else {
                label.setText(fieldViewItem.getTitle().toUpperCase());
            }
            if (fieldViewItem.getFieldType() == FieldViewItem.FieldType.MULTI_LINE) {
                itemValue.setMaxLines(fieldViewItem.getMaxLines());
            } else {
                itemValue.setSingleLine(true);
            }

            itemValue.setText(fieldViewItem.getValue());
            addView(itemLayout, params);
            if (iterator.hasNext()) {
                final View separatorView = new View(getContext());
                separatorView.setBackgroundResource(R.drawable.divider_horizontal_holo_dark);
                addView(separatorView, separatorLayoutParams);
            }
        }
        invalidate();
    }

}
