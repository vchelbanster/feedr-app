package com.chelban.vasile.feedr.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.util.FragmentUtils;

import roboguice.fragment.RoboFragment;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class BaseDataFragment extends RoboFragment implements ActionBarNavigationAware {
    private FragmentUtils fragmentUtils = new FragmentUtils();
    protected NavigationDrawerFragment navigationDrawerFragment;

    @Override
    public boolean isChildScreen() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        navigationDrawerFragment = (NavigationDrawerFragment) fragmentUtils.getCurrentFragment(getFragmentManager(), R.id.navigation_drawer);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (navigationDrawerFragment != null) {
            navigationDrawerFragment.setDrawerIndicatorEnabled(!isChildScreen());
            if (isChildScreen()) {
                navigationDrawerFragment.lockDrawer();
            }
        }
    }

    @Override
    public void onStop() {
        if (navigationDrawerFragment != null && isChildScreen()) {
            navigationDrawerFragment.unlockDrawer();
        }
        super.onStop();
    }

}
