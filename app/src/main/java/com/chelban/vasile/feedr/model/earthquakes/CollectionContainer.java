package com.chelban.vasile.feedr.model.earthquakes;

import java.util.List;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class CollectionContainer {
    private String type;
    private Metadata metadata;
    private List<Feature> features;

    public CollectionContainer() {
    }

    public CollectionContainer(String type, Metadata metadata, List<Feature> features) {
        this.type = type;
        this.metadata = metadata;
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

}
