package com.chelban.vasile.feedr.network;

/**
 * Created by Vasile Chelban on 7/18/2014.
 */
public interface NetworkConsts {
    String EARTHQUAKE_FEED_TYPE = "feed_type";
    String EARTHQUAKES_SERVER_BASE_URL = "http://earthquake.usgs.gov";
    String MUBALOO_SERVER_URL = "http://mubaloo.com/news-info/rss.xml";
    String EARTHQUAKE_GATEWAY_PREFIX = "/earthquakes/feed/v1.0/summary/{" + EARTHQUAKE_FEED_TYPE + "}";
    String EARTHQUAKE_SIGNIFICANT_MONTH_URL = "significant_month.geojson";
    /**
     * 4.5 degrees and up
     */
    String EARTHQUAKE_HIGH_WEEK_URL = "4.5_week.geojson";
    String EARTHQUAKE_ALL_WEEK_URL = "all_week.geojson";
    String EARTHQUAKE_ALL_MONTH_URL = "all_month.geojson";



}
