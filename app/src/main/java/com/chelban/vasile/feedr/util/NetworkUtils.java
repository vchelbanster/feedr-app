package com.chelban.vasile.feedr.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class NetworkUtils {
    /**
     * Checks to see if there is an active network connection.
     *
     * @return <code>true</code> if the user has a network connection, <code>false</code> if not.
     */
    public static boolean isNetworkConnectionAvailable(final Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] info = connectivity.getAllNetworkInfo();
        if (info != null) {
            for (NetworkInfo anInfo : info) {
                if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
