package com.chelban.vasile.feedr.ui.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.util.ImageLoadingUtil;
import com.chelban.vasile.feedr.util.TextUtils;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class GenericListAdapter extends BaseAdapter {
    private List<DataListItem> mListItems;
    @Inject
    private LayoutInflater mLayoutInflater;
    @Inject
    private ImageLoadingUtil mImageLoadingUtil;
    @Inject
    private TextUtils mTextUtils;
    private boolean mHasAnyLocation;
    private boolean locationsChecked;

    public GenericListAdapter() {
        //default c-tor
    }

    public GenericListAdapter(final List<DataListItem> listItems) {
        mListItems = listItems;
    }

    @Override
    public int getCount() {
        return mListItems != null ? mListItems.size() : 0;
    }

    @Override
    public DataListItem getItem(int position) {
        if (mListItems != null && mListItems.size() > position) {
            return mListItems.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.generic_list_item_layout, parent, false);
        }

        Object tag = convertView.getTag();
        final ViewHolder viewHolder = tag != null ? (ViewHolder) tag : new ViewHolder();
        if (tag == null) {
            viewHolder.titleView = (TextView) convertView.findViewById(R.id.item_title);
            viewHolder.descriptionView = (TextView) convertView.findViewById(R.id.item_description);
            viewHolder.dateView = (TextView) convertView.findViewById(R.id.item_date);
            viewHolder.thumbnailView = (ImageView) convertView.findViewById(R.id.item_thumbnail_small);
            viewHolder.hybridThumbnailView = (TextView) convertView.findViewById(R.id.item_hybrid_thumbnail);
        }
        convertView.setTag(viewHolder);

        final DataListItem item = getItem(position);
        viewHolder.dateView.setText(mTextUtils.formatShortDateTime(item.getDateTime()));
        viewHolder.titleView.setText(item.getTitle());
        viewHolder.descriptionView.setText(item.getDetailedDescription());
        //todo: optimise for fast scrolling
        viewHolder.hybridThumbnailView.setText(String.valueOf(item.getHybridThumbChar()));
        viewHolder.hybridThumbnailView.setBackgroundResource(mTextUtils.getBackgroundColorIDForChar(item.getHybridThumbChar()));
        if (item.getThumbnailUri() != null) {
            mImageLoadingUtil.displayImage(item.getThumbnailUri(), new ImageViewAware(viewHolder.thumbnailView),
                    new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            viewHolder.thumbnailView.setVisibility(View.VISIBLE);
                            viewHolder.hybridThumbnailView.setVisibility(View.GONE);
                        }
                    }
            );
        }

        return convertView;
    }

    public List<DataListItem> getListItems() {
        return mListItems;
    }

    public void setListItems(List<DataListItem> listItems) {
        mListItems = listItems;
    }


    public boolean hasAnyLocation() {
        if (!locationsChecked && mListItems != null) {
            for (DataListItem item : mListItems) {
                if (item.getLocation() != null) {
                    mHasAnyLocation = true;
                    break;
                }
            }
        }
        return mHasAnyLocation;
    }

    static class ViewHolder {
        TextView titleView;
        TextView descriptionView;
        TextView dateView;
        ImageView thumbnailView;
        TextView hybridThumbnailView;
    }

}
