package com.chelban.vasile.feedr;

import android.app.Application;

import com.chelban.vasile.feedr.util.ImageLoadingUtil;

/**
 * Created by Vasile Chelban on 7/20/2014.
 */
public class FeedrApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ImageLoadingUtil.initImageLoader(getApplicationContext());
    }
}
