package com.chelban.vasile.feedr.exception;

import android.content.Context;

import com.chelban.vasile.feedr.R;
import com.google.inject.Inject;

/**
 * Exception raised for network-related errors.
 * 
 * Created by Vasile Chelban on 7/20/2014.
 */
public class NetworkException extends Exception {
    @Inject
    public NetworkException(final Context context){
        super(context.getString(R.string.exception_network_msg));
    }

}
