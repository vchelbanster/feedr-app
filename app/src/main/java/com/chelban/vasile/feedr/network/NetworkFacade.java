package com.chelban.vasile.feedr.network;


import com.chelban.vasile.feedr.model.earthquakes.CollectionContainer;
import com.rometools.rome.feed.synd.SyndFeed;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public interface NetworkFacade {

    public void getEarthquakesByType(final EarthquakeFeedType feedType, NetworkResponseListener<CollectionContainer> responseListener);


    public void getFeed(FeedID feedID, NetworkResponseListener<SyndFeed> responseListener);

}
