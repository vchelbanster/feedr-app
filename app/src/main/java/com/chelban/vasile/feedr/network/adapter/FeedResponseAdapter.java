package com.chelban.vasile.feedr.network.adapter;

import com.chelban.vasile.feedr.model.feeds.FeedListItem;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.rometools.rome.feed.synd.SyndEntry;

import java.util.List;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class FeedResponseAdapter extends GenericResponseAdapter<SyndEntry, DataListItem> {

    public FeedResponseAdapter() {
        super();
    }

    public FeedResponseAdapter(List<SyndEntry> syndEntries) {
        super(syndEntries);
    }

    @Override
    public List<DataListItem> getProcessedResponse() {
        for (SyndEntry syndEntry : mInputList) {
            mOutputList.add(new FeedListItem(syndEntry));
        }
        return mOutputList;
    }
}
