package com.chelban.vasile.feedr.ui.fragment;

/**
 * Created by Vasile Chelban on 7/20/2014.
 */
public enum DataSourceType {
    EARTHQUAKES, MUBALOO_FEED
}
