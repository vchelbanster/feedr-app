package com.chelban.vasile.feedr.network.adapter;

import com.chelban.vasile.feedr.model.earthquakes.Feature;
import com.chelban.vasile.feedr.ui.adapter.DataListItem;

import java.util.List;

/**
 * Created by Vasile Chelban on 7/23/2014.
 */
public class EarthquakesResponseAdapter extends GenericResponseAdapter<Feature, DataListItem> {

    public EarthquakesResponseAdapter() {
        super();
    }

    public EarthquakesResponseAdapter(List<Feature> featureList) {
        super(featureList);
    }

    @Override
    public List<DataListItem> getProcessedResponse() {
        for (Feature feature : mInputList) {
            mOutputList.add(feature);
        }
        return mOutputList;
    }
}
