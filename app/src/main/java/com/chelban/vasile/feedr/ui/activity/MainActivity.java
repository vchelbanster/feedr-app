package com.chelban.vasile.feedr.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.chelban.vasile.feedr.R;
import com.chelban.vasile.feedr.ui.fragment.ActionBarNavigationAware;
import com.chelban.vasile.feedr.ui.fragment.DataSourceType;
import com.chelban.vasile.feedr.ui.fragment.GenericListFragment;
import com.chelban.vasile.feedr.ui.fragment.NavigationDrawerFragment;
import com.chelban.vasile.feedr.util.FragmentUtils;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import roboguice.RoboGuice;


public class MainActivity extends RoboActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks{

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private FragmentUtils fragmentUtils = new FragmentUtils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);


        mNavigationDrawerFragment = (NavigationDrawerFragment) fragmentUtils.getCurrentFragment(getSupportFragmentManager(), R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        mTitle = getTitle();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        DataSourceType dataSourceType = DataSourceType.EARTHQUAKES;
        final String[] titles = getResources().getStringArray(R.array.navigation_options_titles);
        if (position < titles.length) {
            mTitle = titles[position];
        }
        switch (position) {
            case NavigationDrawerFragment.EARTHQUAKES_NAV: {
                dataSourceType = DataSourceType.EARTHQUAKES;
                break;
            }
            case NavigationDrawerFragment.MUBALOO_FEED_NAV: {
                dataSourceType = DataSourceType.MUBALOO_FEED;
                break;
            }
        }
        Fragment currentFragment = fragmentUtils.getCurrentFragment(getSupportFragmentManager(), R.id.main_container);
        if (currentFragment == null || (currentFragment instanceof GenericListFragment &&
                ((GenericListFragment) currentFragment).getSourceType() != dataSourceType)) {
            final GenericListFragment fragment = RoboGuice.getInjector(getApplicationContext()).getInstance(GenericListFragment.class);
            fragment.setSourceType(dataSourceType);
            currentFragment = fragment;
        }
        fragmentUtils.attachFragment(getSupportFragmentManager(), R.id.main_container, currentFragment, false);
        setTitle(mTitle);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            Fragment currentFragment = fragmentUtils.getCurrentFragment(getSupportFragmentManager(), R.id.main_container);
            boolean globalActionBarAvailability = (!(currentFragment instanceof ActionBarNavigationAware)) ||
                    (!((ActionBarNavigationAware) currentFragment).isChildScreen());

            if (globalActionBarAvailability) {
                getMenuInflater().inflate(R.menu.main, menu);
                restoreActionBar();
            }
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        Crouton.cancelAllCroutons();
        super.onDestroy();
    }

}
