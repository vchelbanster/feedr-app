package com.chelban.vasile.feedr.util;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.format.DateUtils;

import com.chelban.vasile.feedr.R;
import com.google.inject.Inject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vasile Chelban on 7/20/2014.
 */
public final class TextUtils {
    private static final String TODAY_TIME_FORMAT = "k:mm";
    private static final String SHORT_DATE_FORMAT = "d MMM";
    private static final String LONG_DATE_FORMAT = "d MMMM";
    private TypedArray mColorArray;
    @Inject
    private Resources mResources;
    private Map<Character, Integer> charColorMap;

    public String formatShortDateTime(final long time) {
        SimpleDateFormat sdf;
        if (DateUtils.isToday(time)) {
            sdf = new SimpleDateFormat(TODAY_TIME_FORMAT);
        } else {
            sdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
        }
        return sdf.format(new Date(time));
    }

    public String formatLongDateTime(final long time) {
        SimpleDateFormat sdf;
        if (DateUtils.isToday(time)) {
            sdf = new SimpleDateFormat(TODAY_TIME_FORMAT);
        } else {
            sdf = new SimpleDateFormat(LONG_DATE_FORMAT);
        }
        return sdf.format(new Date(time));
    }

    public int getBackgroundColorIDForChar(Character character) {
        final int colorID;
        if (charColorMap == null) {
            charColorMap = new HashMap<Character, Integer>();
        }
        if (charColorMap.containsKey(character)) {
            colorID = charColorMap.get(character);
        } else {
            final int defaultIndex = 0;
            if (mColorArray == null) {
                mColorArray = mResources.obtainTypedArray(R.array.thumb_bg_colors);
            }
            int numericValue = Character.getNumericValue(character);
            if (numericValue < 10 || numericValue > 35) {
                numericValue = defaultIndex;
            } else {
                numericValue -= 10;
                numericValue /= 2;
            }
            final int colorCount = mColorArray.length();
            final int resourceIndex = numericValue < colorCount - 1 ? numericValue : defaultIndex;
            colorID = mColorArray.getResourceId(resourceIndex, defaultIndex);
            charColorMap.put(character, colorID);
        }

        return colorID;
    }
}
