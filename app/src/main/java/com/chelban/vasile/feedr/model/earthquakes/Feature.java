package com.chelban.vasile.feedr.model.earthquakes;

import com.chelban.vasile.feedr.ui.adapter.DataListItem;
import com.chelban.vasile.feedr.ui.view.FieldViewItem;
import com.chelban.vasile.feedr.util.TextUtils;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasile Chelban on 7/19/2014.
 */
public class Feature implements DataListItem {
    private String id;
    private String type;
    private FeatureProperties properties;
    @SerializedName("geometry")
    private FeatureGeometry featureGeometry;

    private List<FieldViewItem> fieldItems;

    private Character hybridThumbnailChar;
    private String description;


    public Feature() {
        fieldItems = new ArrayList<FieldViewItem>();
    }

    public Feature(String id, String type, FeatureProperties properties) {
        this.id = id;
        this.type = type;
        this.properties = properties;
        fieldItems = new ArrayList<FieldViewItem>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FeatureProperties getProperties() {
        return properties;
    }

    public void setProperties(FeatureProperties properties) {
        this.properties = properties;
    }

    @Override
    public String getTitle() {
        return properties.getTitle();
    }

    @Override
    public String getDetailedDescription() {
        if (description == null) {
            StringBuilder stringBuilder = new StringBuilder();
            if (properties.getAlert() != null) {
                stringBuilder.append("Alert: ").append(properties.getAlert()).append("  ");
            }
            stringBuilder.append("Felt by ");
            if (properties.getFelt() > 0) {
                stringBuilder.append(properties.getFelt()).append(" people\n");
            } else {
                stringBuilder.append("no one\n");
            }
            stringBuilder.append("Place: ").append(properties.getPlace());


            description = stringBuilder.toString();
        }
        return description;
    }

    @Override
    public String getThumbnailUri() {
        return null;
    }

    @Override
    public long getDateTime() {
        return properties.getTime();
    }

    @Override
    public Character getHybridThumbChar() {
        if (hybridThumbnailChar == null) {
            final String place = properties.getPlace() != null ? properties.getPlace() : properties.getTitle();
            int position = place.indexOf("of") + 2;
            hybridThumbnailChar = place.charAt(position);
            while (!Character.isLetter(hybridThumbnailChar) && position + 1 < place.length()) {
                hybridThumbnailChar = place.charAt(++position);
            }
            if (!Character.isLetter(hybridThumbnailChar)) {
                hybridThumbnailChar = place.charAt(0);
            }

        }
        return hybridThumbnailChar;
    }

    @Override
    public Coordinate getLocation() {
        return featureGeometry != null ? featureGeometry.getFeatureCoordinates() : null;
    }

    @Override
    public List<FieldViewItem> getItemFields() {
        if (fieldItems.size() == 0) {
            if (!android.text.TextUtils.isEmpty(properties.getAlert())) {
                fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_ALERT, properties.getAlert(), FieldViewItem.FieldType.SINGLE_LINE));
            }
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_MAG, String.valueOf(properties.getMag()) + " degrees",
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_PLACE, properties.getPlace(),
                    FieldViewItem.FieldType.MULTI_LINE, 3));
            final TextUtils textUtils = new TextUtils();
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_TIME, textUtils.formatLongDateTime(properties.getTime()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_UPDATED_TIME, textUtils.formatLongDateTime(properties.getUpdated()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            final String feltStr = "Felt by " + ((properties.getFelt() > 0) ? (String.valueOf(properties.getFelt()) + " people\n") : "no one");

            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_FELT, feltStr, FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_TSUNAMI, properties.getTsunami() == 1 ? "YES" : "NO",
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_CDI, String.valueOf(properties.getCdi()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_CDI, String.valueOf(properties.getCdi()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_NST, String.valueOf(properties.getNst()) + " stations",
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_STATUS, properties.getStatus(),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_SIG, String.valueOf(properties.getSig()),
                    FieldViewItem.FieldType.SINGLE_LINE));//todo: add progress bar to show level
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_MMI, String.valueOf(properties.getMmi()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_NET, String.valueOf(properties.getNet()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_CODE, String.valueOf(properties.getCode()),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_TYPE, properties.getType(),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_DMIN, String.valueOf(properties.getDmin()) + " degrees",
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_MAG_TYPE, properties.getMagType(),
                    FieldViewItem.FieldType.SINGLE_LINE));
            fieldItems.add(new FieldViewItem(FeatureProperties.FIELD_SOURCES, properties.getSources(),
                    FieldViewItem.FieldType.MULTI_LINE, 2));
        }

        return fieldItems;
    }

    public FeatureGeometry getFeatureGeometry() {
        return featureGeometry;
    }

    public void setFeatureGeometry(FeatureGeometry featureGeometry) {
        this.featureGeometry = featureGeometry;
    }
}
