# Configuration for ProGuard
# From http://proguard.sourceforge.net/index.html#/manual/examples.html#androidapplication
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-ignorewarnings

-keepattributes SourceFile,LineNumberTable,*Annotation*,Signature
-allowaccessmodification
-renamesourcefileattribute SourceFile
-repackageclasses ''

-keep class android.support.v4.** { *; }
-keep class android.** { *; }
-keep class com.google.gson.** {*;}

-keep class roboguice.** { *; }
-keep class org.apache.** { *; }
-keep class com.google.inject.** { *; }
-keep class net.jcip.annotations.** { *; }
-keep class de.keyboardsurfer.android.widget.crouton.** { *; }
-keep class com.chelban.vasile.feedr.di.** {*;}

-keep class com.google.inject.Binder
-keep public class com.google.inject.Inject
-keep class **$$ViewInjector { *; }

-keep class javax.inject.** { *; }
-keep class javax.annotation.** { *; }

-dontwarn android.support.v4.**
-dontwarn roboguice.**

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment
-keep public class * extends com.google.inject.AbstractModule
-keep public class com.android.vending.licensing.ILicensingService
-keep class com.google.appengine.api.urlfetch.** {*;}
-keep class org.jaxen.** {*;}
-keep class javax.xml.stream.** {*;}
-keep class org.apache.tools.ant.** {*;}
-keep class java.beans.** {*;}
-keep class org.objectweb.asm.** {*;}
-keep class com.squareup.okhttp.** {*;}
-keep class rx.** {*;}
-keep class rg.slf4j.** {*;}
-dontnote com.android.vending.licensing.ILicensingService

-keep public class com.endava.agility.entities.** {
     public void set*(***);
     public boolean is*();
     public *** get*();
}


-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


-keepclassmembers class * extends android.app.Activity{
    public <methods>;
}


-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public interface com.android.vending.licensing.ILicensingService

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclassmembers class * extends java.lang.Enum {
    public static **[] values();
    public static ** valueOf(java.lang.String);
    public static ** fromTypeString(java.land.String);
}

-keepclassmembers class * {
    void *(**On*Event);
}

-keepclassmembers class * {
	@com.google.inject.Inject <init>(...);
	@com.google.inject.Inject <fields>;
	@javax.annotation.Nullable <fields>;
}

-keep @roboguice.inject.ContentView public class *


-keepclassmembers class * {
	void *(roboguice.activity.event.*);
}

# Removes all calls to Log. Delete the methods you want to keep.
-assumenosideeffects class android.util.Log {
    public static int v(...);
    public static int d(...);
    public static int i(...);
    public static int w(...);
    public static int e(...);
    public static int wtf(...);
}
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.chelban.vasile.model.** { *; }


#===Google Play Services
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

